<!-- @format -->

- theme delay:
  useColorMode()

colorMode at first: will take take value as the initialColorMode, after that will get the selected value from localStorage which will cause delay in coloring

- media query delay (the same goes for it)
  useMediaQuery()

  one way to solve this: do NOT use useMediaQuery(), instead the in components props

  ex: <Grid templateColumns={{ base: `repeat(1, 1fr)`, sm: `repeat(2, 1fr)`, lg: `repeat(4, 1fr)` }}> </Grid>

<br>
<br>
<br>

- get red of @format at the top of every page by using prettier
