// ** React
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

// ** Layouts
import WelcomeLayout from './layout/WelcomeLayout';

// ** Pages
import Home from './pages/Home';
import GamePreview from './pages/PreviewGame';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import type { StyleFunctionProps } from '@chakra-ui/styled-system';
import { mode } from '@chakra-ui/theme-tools';
import {
    extendTheme,
    type ThemeConfig,
    ChakraProvider,
} from '@chakra-ui/react';

const config: ThemeConfig = {
    initialColorMode: 'light',
    useSystemColorMode: true,
};

const theme = extendTheme({
    config,
    styles: {
        global: (props: StyleFunctionProps) => ({
            body: {
                color: mode('#111111', '#fefefe')(props),
                bg: mode('#fefefe ', '#111111')(props),
            },
        }),
    },
});

const queryClient = new QueryClient();

const router = createBrowserRouter([
    {
        element: <WelcomeLayout />,
        children: [
            { path: '/', element: <Home /> },
            { path: '/games/:id', element: <GamePreview /> },
        ],
    },
    {
        path: '/*',
        element: <div>not fount | 404 </div>,
    },
]);

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <ChakraProvider theme={theme}>
                <RouterProvider router={router} />;
            </ChakraProvider>
        </QueryClientProvider>
    );
}

export default App;
