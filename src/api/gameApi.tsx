// ** React
import { useContext } from 'react';
import { GamesFilterContext } from '@/context/GamesFilterContext';
import { useInfiniteQuery, useQuery } from '@tanstack/react-query';

import { generateNewURLParams } from '@/helpers/generateNewURLParams';
import { fetchData } from '@/helpers/fetchData';

import { GenreType } from './genreApi';
import { insertToString } from '@/helpers/inserToString';

const API_KEY = import.meta.env.VITE_API_KEY;

type Clip = {
    clip: string;
    clips: { 320: string; 640: string; full: string }[];
    preview: string;
    video: string;
};

export type GameFilters = {
    page?: string;
    page_size?: string;
    genres?: string;
    platforms?: string;
    ordering?: string;
    search?: string;
};

export type Platform = {
    id: number;
    name: string;
    slug:
        | 'pc'
        | 'playstation'
        | 'xbox'
        | 'nintendo'
        | 'mac'
        | 'linux'
        | 'android'
        | 'ios'
        | 'web';
    image_background?: string;
};
export type GameType = {
    id: number;
    slug: string;
    name: string;
    background_image: string;
    rating: number;
    platforms: {
        platform: Platform;
    }[];
    parent_platforms: {
        platform: Platform;
    }[];
    genres: GenreType[];
    clip: Clip;
    playtime: string;
    description: string;
};

export type TrailerType = {
    id: string;
    name: string;
    preview: string;
    data: {
        '400': string;
        max: string;
    };
};

export const useGetGames = () => {
    const { filters } = useContext(GamesFilterContext);

    const query = ({ pageParam }: any) => {
        const UrlParams = generateNewURLParams({
            key: API_KEY,
            ...filters,
            page: pageParam,
        });

        return fetchData('games', UrlParams);
    };

    const { data, isError, fetchNextPage, isFetching, hasNextPage } =
        useInfiniteQuery({
            queryKey: ['games', filters],
            queryFn: query,
            initialPageParam: 1,
            getNextPageParam: (lastPage) => {
                if (lastPage.next) return lastPage.currentPage + 1;
            },
        });

    const games: GameType[] = data?.pages.reduce((acc, currentPage) => {
        let results: GameType[] = currentPage.results;

        results = results.map((result) => ({
            ...result,
            background_image: insertToString(
                result.background_image ?? '',
                'crop/600/400',
            ),
        }));
        return [...acc, ...results];
    }, []);

    return {
        games,
        isError,
        isFetching,
        fetchNextPage,
        hasNextPage,
    };
};

export const useGetGameById = (id: string) => {
    const query = () => {
        const URLparams = generateNewURLParams({ key: API_KEY });

        return fetchData(`games/${id}`, URLparams);
    };

    const { data, isLoading, isError } = useQuery({
        queryKey: ['games', id],
        queryFn: query,
    });

    const game: GameType = data;

    return { game, isLoading, isError };
};
