// ** React
import { useQuery } from '@tanstack/react-query';

import { fetchData } from '@/helpers/fetchData';
import { generateNewURLParams } from '@/helpers/generateNewURLParams';
import { insertToString } from '@/helpers/inserToString';

const API_KEY = import.meta.env.VITE_API_KEY;

export type GenreType = {
    id: number;
    name: string;
    image_background: string;
};

export const useGetGenres = () => {
    const query = () => {
        const URLparams = generateNewURLParams({ key: API_KEY });

        return fetchData('genres', URLparams);
    };

    const { data, isLoading, isError } = useQuery({
        queryKey: ['genres'],
        queryFn: query,
    });

    const genres: GenreType[] = data?.results.map((result: GenreType) => ({
        ...result,
        image_background: insertToString(
            result.image_background,
            'crop/600/400',
        ),
    }));

    return { genres, isLoading, isError };
};
