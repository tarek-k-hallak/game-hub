// ** Components
import { Container as ChakraContainer } from '@chakra-ui/react';

type Props = {
    children: React.ReactNode;
    [key: string]: any;
};

const Container = ({ children, ...restProps }: Props) => {
    return (
        <ChakraContainer maxWidth={'8xl'} {...restProps}>
            {children}
        </ChakraContainer>
    );
};

export default Container;
