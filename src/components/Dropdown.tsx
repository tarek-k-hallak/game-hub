import { Select } from 'chakra-react-select';

type option = { value: number | string; label: string };

type Props = {
    placeholder: string;
    options: option[];
    onChange: (option: any) => void;
    value: option | undefined;
    isClearable?: boolean;
};

const Dropdown = ({
    options,
    onChange,
    value,
    placeholder,
    isClearable = true,
    ...rest
}: Props) => {
    return (
        <Select
            placeholder={placeholder}
            isClearable={isClearable}
            value={!!value ? value : null}
            options={options}
            onChange={onChange}
            {...rest}
        />
    );
};

export default Dropdown;
