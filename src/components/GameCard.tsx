import { GameType } from '@/api/gameApi';

// ** Components
import {
    Card,
    CardBody,
    Image,
    Text,
    Skeleton,
    AspectRatio,
    Collapse,
    useDisclosure,
    CardFooter,
    Flex,
    Badge,
    Link,
} from '@chakra-ui/react';
import fallbackImage from '@/assets/fallback-image.webp';
import PlatformIcon from './PlatformIcon';

type Props = {
    game: GameType;
};

export const GameCard = ({ game }: Props) => {
    const { isOpen, onToggle } = useDisclosure();

    return (
        <Card
            position={'relative'}
            boxShadow='2xl'
            borderRadius={'lg'}
            onMouseEnter={onToggle}
            onMouseLeave={onToggle}
        >
            <AspectRatio ratio={3 / 2}>
                <Image
                    src={game.background_image}
                    boxSize='fit-content'
                    borderTopRadius={'lg'}
                    objectFit='cover'
                    fallbackSrc={fallbackImage}
                    alt={game.name}
                />
            </AspectRatio>

            <CardBody height={8} position={'relative'}>
                <Flex justifyContent={'space-between'} mb={4}>
                    <Flex gap={2}>
                        {game.parent_platforms.map((platform) => {
                            return (
                                <PlatformIcon
                                    key={platform.platform.id}
                                    slug={platform.platform.slug}
                                />
                            );
                        })}
                    </Flex>
                    <Badge colorScheme={'green'}>{game.rating}</Badge>
                </Flex>
                <Text
                    as={'b'}
                    fontSize={'2xl'}
                    lineHeight={'1'}
                    cursor={'pointer'}
                    _hover={{ color: '#cacaca' }}
                >
                    <Link href={`/games/${game.slug}`}>{game.name}</Link>
                </Text>
            </CardBody>

            <Collapse
                in={isOpen}
                transition={{
                    exit: { duration: 0.5 },
                    enter: { duration: 0.5 },
                }}
            >
                <CardFooter>test</CardFooter>
            </Collapse>
        </Card>
    );
};

GameCard.Skeleton = () => {
    return (
        <Card borderRadius={'lg'} boxShadow='2xl'>
            <AspectRatio ratio={3 / 2}>
                <Skeleton height='10rem' borderTopRadius={'lg'} />
            </AspectRatio>
            <CardBody>
                <Flex justifyContent={'space-between'} mb={4}>
                    <Flex gap={2}>
                        <Skeleton width='1rem' height={'1rem'} />
                        <Skeleton width='1rem' height={'1rem'} />
                        <Skeleton width='1rem' height={'1rem'} />
                        <Skeleton width='1rem' height={'1rem'} />
                    </Flex>
                    <Skeleton width='1rem' height={'1rem'} />
                </Flex>
                <Skeleton height='20px' />
            </CardBody>
        </Card>
    );
};

export default GameCard;
