import { Flex, Stack, Text, Skeleton, Box } from '@chakra-ui/react';

type Props = {
    children: React.ReactNode;
};

const List = ({ children }: Props) => {
    return <Stack gap={5}>{children}</Stack>;
};

const Header = ({ children }: Props) => {
    return (
        <Text as={'b'} fontSize='2xl'>
            {children}
        </Text>
    );
};

const Item = ({ children }: Props) => {
    return <Box>{children}</Box>;
};

const ItemsSkeleton = () => {
    return Array(8)
        .fill(null)
        .map((_, idx) => (
            <Flex
                key={idx}
                direction={'row'}
                alignItems={'center'}
                gap={'1rem'}
            >
                <Skeleton height='1.6rem' w='1.6rem' />
                <Skeleton height='1rem' flex='1' />
            </Flex>
        ));
};

List.Header = Header;
List.Item = Item;

export { List, ItemsSkeleton };
