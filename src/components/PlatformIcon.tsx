import { useColorMode } from '@chakra-ui/react';
import { Icon } from '@iconify/react';

const platformsIcons: { [key: string]: string } = {
    pc: 'streamline:windows-solid',
    playstation: 'cib:playstation',
    xbox: 'ri:xbox-fill',
    nintendo: 'SiNintendo',
    mac: 'wpf:mac-os',
    linux: 'uil:linux',
    android: 'fontisto:android',
    ios: 'material-symbols:ios',
    web: 'mdi:web',
};

type Props = {
    slug: string;
    width?: number;
};

const PlatformIcon = ({ slug, width }: Props) => {
    const { colorMode } = useColorMode();

    return (
        <Icon
            color={colorMode === 'dark' ? '#fefefe' : '#111111'}
            icon={platformsIcons[slug]}
            width={`${width || 1}rem`}
        />
    );
};

export default PlatformIcon;
