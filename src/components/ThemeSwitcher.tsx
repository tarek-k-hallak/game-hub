import { useColorMode } from '@chakra-ui/react';

import { Switch } from '@chakra-ui/react';

export default function ThemeSwitcher() {
    const { toggleColorMode, colorMode } = useColorMode();

    return (
        <Switch isChecked={colorMode === 'dark'} onChange={toggleColorMode} />
    );
}
