import { createContext, useCallback } from 'react';
import { useSearchParams } from 'react-router-dom';
import { GameFilters } from '@/api/gameApi';

import debounce from 'lodash.debounce';

interface GamesFilterContextProps {
    filters: GameFilters;
    setFilter: (name: string, value: string) => void;
}

export const GamesFilterContext = createContext<GamesFilterContextProps>({
    filters: {},
    setFilter: () => {},
});

export const GamesFilterProvider = ({ children }: any) => {
    const [searchParams, setSearchParams] = useSearchParams();
    const filters: GameFilters = {
        search: searchParams.get('search') || '',
        genres: searchParams.get('genres') || '',
        ordering: searchParams.get('ordering') || '',
        platforms: searchParams.get('platforms') || '',
    };

    const updateParams = (name: string, value: string) => {
        setSearchParams(
            (prev) => {
                if (value) prev.set(name, value);
                else prev.delete(name);
                return prev;
            },
            { replace: true },
        );
    };

    const debounceSearch = useCallback(
        debounce((name: string, value: string) => {
            updateParams(name, value);
        }, 1000),
        [updateParams],
    );

    const updateFilter = (name: string, value: string) => {
        if (name === 'search') {
            debounceSearch(name, value);
        } else {
            updateParams(name, value);
        }
    };

    return (
        <GamesFilterContext.Provider
            value={{ filters, setFilter: updateFilter }}
        >
            {children}
        </GamesFilterContext.Provider>
    );
};
