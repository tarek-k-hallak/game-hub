import { enqueueSnackbar } from "notistack";

function HandleHttpError(e: any) {
  console.log(e);
  enqueueSnackbar(e.response?.data?.message ?? "Something went wrong", {
    variant: "error",
  });
}

export default HandleHttpError;
