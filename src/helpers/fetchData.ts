const API_BASE_URL = import.meta.env.VITE_API_BASE_URL;

export const fetchData = async (path: string, UrlParams: any) => {
    const response = await fetch(`${API_BASE_URL}/${path}?${UrlParams}`);
    const data = await response.json();

    data.currentPage = +UrlParams.get('page');

    return data;
};
