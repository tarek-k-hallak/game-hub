type FiltersType = { [key: string]: string };

export const generateNewURLParams = (filters: FiltersType) => {
    const UrlParams = new URLSearchParams();

    Object.keys(filters).map((key) => {
        if (filters[key]) UrlParams.set(key, filters[key]);
    });

    return UrlParams;
};
