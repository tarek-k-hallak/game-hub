export const insertToString = (currentString: string, newPart: string) => {
    if (!currentString) {
        return '';
    }
    const parts = currentString.split('/');
    const mediaIndex = parts.indexOf('media');

    if (mediaIndex !== -1) {
        parts.splice(mediaIndex + 1, 0, newPart);
        return parts.join('/');
    }

    // If "media" is not found in the string, return the original text
    return currentString;
};
