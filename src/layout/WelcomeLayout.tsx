// ** React
import { Outlet } from 'react-router-dom';
import { GamesFilterProvider } from '@/context/GamesFilterContext';

// ** Views
import Footer from '@/views/shared/Footer';
import Header from '@/views/shared/Header';

// ** Components
import { SnackbarProvider } from 'notistack';

const SnackBar = () => {
    return (
        <SnackbarProvider
            anchorOrigin={{
                horizontal: 'right',
                vertical: 'top',
            }}
            SnackbarProps={{
                style: {
                    top: '5vw',
                },
            }}
            maxSnack={1}
        />
    );
};

export default function WelcomeLayout() {
    return (
        <GamesFilterProvider>
            <SnackBar />
            <Header />
            <Outlet />
            <Footer />
        </GamesFilterProvider>
    );
}
