// ** Views
import Aside from '@/views/homePage/Aside';
import Content from '@/views/homePage/Content';

// ** Components
import { Box, Flex, Show } from '@chakra-ui/react';
import Container from '@/components/Container';

const Home = () => {
    return (
        <Box minHeight={'100vh'} pt={'7rem'}>
            <Container>
                <Flex direction={'row'} gap={8}>
                    <Show breakpoint='(min-width: 991px)'>
                        <Box as='aside' w={'10rem'}>
                            <Aside />
                        </Box>
                    </Show>
                    <Box as='main' flex='1'>
                        <Content />
                    </Box>
                </Flex>
            </Container>
        </Box>
    );
};

export default Home;
