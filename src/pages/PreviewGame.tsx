import { Navigate, useParams } from 'react-router-dom';

import Hero from '@/views/previewGamePage/Hero';
import Content from '@/views/previewGamePage/Content';

import { Box } from '@chakra-ui/react';
import { useGetGameById } from '@/api/gameApi';

const GamePreview = () => {
    const { id = '' } = useParams();
    const { game, isError, isLoading } = useGetGameById(id);

    if (!id) {
        return Navigate({ to: '/not-found', replace: true });
    }

    return (
        <Box>
            <Hero game={game} isLoading={isLoading} isError={isError} />
            <Content game={game} isLoading={isLoading} isError={isError} />
        </Box>
    );
};

export default GamePreview;
