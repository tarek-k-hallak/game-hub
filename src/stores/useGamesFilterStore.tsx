import { create } from 'zustand';
import { querystring } from 'zustand-querystring';

interface GameFilters {
    page?: string;
    page_size?: string;
    genres?: string;
    platforms?: string;
    ordering?: string;
    search?: string;
}
type GamesStoreType = {
    filters: GameFilters;
    setFilter: (name: string, value: string) => void;
};

export const useGamesFilterStore = create<GamesStoreType>()(
    querystring((set, get) => ({
        filters: {
            page: '1',
        },
        setFilter(name, value) {
            const currentFilters = get().filters;
            set({ filters: { ...currentFilters, [name]: value } });
        },
    })),
);
