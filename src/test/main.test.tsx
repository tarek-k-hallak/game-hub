import { GameType } from '@/api/gameApi';
import { GamesGrid } from '@/views/homePage/Content';
import { render, screen } from '@testing-library/react';

describe('Games Grid', () => {
    it('should render "no..." when there is no games to show!', () => {
        render(<GamesGrid games={[]} isError={false} isFetching={false} />);
        screen.debug();
        expect(screen.getByText(/no/i)).toBeInTheDocument();
    });

    it('should render "error" when there is an error fetching games', () => {
        render(<GamesGrid games={[]} isError={true} isFetching={false} />);
        screen.debug();
        expect(screen.getByText(/error/i)).toBeInTheDocument();
    });

    it('should render Gta when successfully fetching games', () => {
        const games: GameType[] = [
            {
                id: 1,
                background_image: '',
                name: 'Gta',
                genres: [{ id: 1, image_background: '', name: '' }],
                rating: 1,
                platforms: [
                    { platform: { id: 1, image_background: '', name: '' } },
                ],
                parent_platforms: [{ platform: { id: 1, name: '', slug: '' } }],
            },
        ];

        render(<GamesGrid games={games} isError={false} isFetching={false} />);

        screen.debug();
        expect(screen.getByText(/gta*/i)).toBeInTheDocument();
    });
});
