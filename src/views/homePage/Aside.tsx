// ** React
import { useContext } from 'react';
import { GamesFilterContext } from '@/context/GamesFilterContext';

// ** APIs
import { useGetGenres, GenreType } from '@/api/genreApi';

// ** Components
import { Flex, Image, Text } from '@chakra-ui/react';
import { List, ItemsSkeleton } from '@/components/List';
import fallbackImage from '@/assets/fallback-image.webp';
const Genres = () => {
    // ** States
    const { genres, isLoading, isError } = useGetGenres();
    const { setFilter, filters } = useContext(GamesFilterContext);

    // ** Functions
    const handleChangeFilter = (name: string, value: string) => {
        setFilter(name, value);
    };

    return (
        <List>
            <List.Header>Genres</List.Header>
            {isLoading && <ItemsSkeleton />}
            {isError && <div> error fetching genres</div>}
            {!genres || (genres.length === 0 && <div> No genres found</div>)}
            {genres?.map((genre: GenreType) => (
                <List.Item key={genre.id}>
                    <Flex
                        gap={2}
                        cursor={'pointer'}
                        alignItems={'center'}
                        textDecoration={
                            filters.genres === genre.id.toString()
                                ? 'underline'
                                : ''
                        }
                        _hover={{ textDecoration: 'underline' }}
                    >
                        <Image
                            boxSize='2rem'
                            borderRadius={'lg'}
                            objectFit='cover'
                            aspectRatio={4 / 3}
                            src={genre.image_background}
                            fallbackSrc={fallbackImage}
                            alt={genre.name}
                        />
                        <Text
                            onClick={() =>
                                handleChangeFilter(
                                    'genres',
                                    genre.id.toString(),
                                )
                            }
                        >
                            {genre.name}
                        </Text>
                    </Flex>
                </List.Item>
            ))}
        </List>
    );
};

const Aside = () => {
    return (
        <>
            {/* <NewRelease /> */}
            <Genres />
            {/* <Top /> */}
        </>
    );
};

export default Aside;
