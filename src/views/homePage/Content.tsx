// ** React
import { useContext, useEffect } from 'react';
import { GamesFilterContext } from '@/context/GamesFilterContext';
import { useInView } from 'react-intersection-observer';

// ** Components
import { Box, Flex, Grid, SlideFade, Text } from '@chakra-ui/react';
import GameCard from '@/components/GameCard';
import Dropdown from '@/components/Dropdown';

// ** APIs
import { useGetGames, GameType } from '@/api/gameApi';

export default function Content() {
    // ** states
    const { ref: inViewRef, inView } = useInView();

    // ** Hooks
    const { games, isError, fetchNextPage, isFetching, hasNextPage } =
        useGetGames();

    useEffect(() => {
        if (inView && !isFetching && hasNextPage) {
            fetchNextPage();
        }
    }, [fetchNextPage, inView, isFetching, , hasNextPage]);

    return (
        <Flex direction={'column'} gap={8}>
            <Box>
                <Text as={'b'} fontSize={'5xl'}>{`New and trending`}</Text>
                <Text
                    fontSize={'xl'}
                    my={2}
                >{`Based on player counts and release date`}</Text>
            </Box>

            <GamesGridToolBar />

            <GamesGrid
                games={games}
                isError={isError}
                isFetching={isFetching}
            />

            <Box h={0} ref={inViewRef} />
        </Flex>
    );
}

export const GamesGrid = ({
    games,
    isError,
    isFetching,
}: {
    games: GameType[] | undefined;
    isError: boolean;
    isFetching: boolean;
}) => {
    const renderContent = () => {
        if (isError) {
            return <div> Error fetching games</div>;
        } else if ((games && games.length > 0) || isFetching) {
            return (
                <>
                    {games?.map((game: GameType, idx: number) => (
                        <SlideFade
                            key={game.id}
                            in={true}
                            offsetY='20px'
                            delay={(idx % 8) / 8}
                        >
                            <GameCard game={game} />
                        </SlideFade>
                    ))}
                    {isFetching &&
                        Array(8)
                            .fill(null)
                            .map((_, idx) => <GameCard.Skeleton key={idx} />)}
                </>
            );
        } else {
            return <div> No games found!</div>;
        }
    };

    return (
        <Grid
            templateColumns={{
                base: `repeat(1, 1fr)`,
                sm: `repeat(2, 1fr)`,
                md: `repeat(3, 1fr)`,
                lg: `repeat(4, 1fr)`,
            }}
            gap={8}
        >
            {renderContent()}
        </Grid>
    );
};

export const GamesGridToolBar = () => {
    const { filters, setFilter } = useContext(GamesFilterContext);

    const platforms = [
        { value: '1', label: 'Xbox' },
        { value: '3', label: 'iOS' },
        { value: '4', label: 'Pc' },
        { value: '5', label: 'macOS' },
        { value: '6', label: 'Linux' },
    ];

    const orderings = [
        { value: 'name', label: 'Name' },
        { value: 'released', label: 'Released' },
        { value: 'added', label: 'Added' },
        { value: 'updated', label: 'Updated' },
        { value: 'rating', label: 'Rating' },
    ];

    const platformCurrentValue = platforms.find(
        (platform) => platform.value === filters.platforms,
    );
    const orderingCurrentValue = orderings.find(
        (ordering) => ordering.value === filters.ordering,
    );

    return (
        <Flex gap={[2, 5, 8]}>
            <Box w={[150, 175, 300]}>
                <Dropdown
                    placeholder={'Platforms'}
                    value={platformCurrentValue}
                    options={platforms}
                    onChange={(obj) =>
                        setFilter('platforms', obj?.value.toString())
                    }
                />
            </Box>

            <Box w={[175, 200, 400]}>
                <Dropdown
                    placeholder={'Ordering'}
                    value={orderingCurrentValue}
                    options={orderings}
                    onChange={(option) =>
                        setFilter('ordering', option?.value.toString())
                    }
                />
            </Box>
        </Flex>
    );
};
