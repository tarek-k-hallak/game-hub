import { GameType } from '@/api/gameApi';

type Props = {
    game: GameType;
    isLoading: boolean;
    isError: boolean;
};
const Content = ({ game, isLoading, isError }: Props) => {
    if (isLoading) {
        return <div>loading...</div>;
    }

    if (isError) {
        return <div>error fetching game details</div>;
    }

    return <div>{game.description}</div>;
};

export default Content;
