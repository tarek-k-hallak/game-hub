import { GameType } from '@/api/gameApi';
import { Box, Flex, HStack, Text, VStack } from '@chakra-ui/react';
import { useRef } from 'react';
import Container from '@/components/Container';
import PlatformIcon from '@/components/PlatformIcon';

type Props = {
    game: GameType;
    isLoading: boolean;
    isError: boolean;
};

const Hero = ({ game, isLoading, isError }: Props) => {
    const videoRef = useRef<HTMLVideoElement>(null);

    if (isLoading) {
        return (
            <Box minHeight={'100vh'} position={'relative'} pt={32}>
                <Container>
                    <Flex
                        flexDirection={{ base: 'column-reverse', lg: 'row' }}
                        justifyContent={'space-between'}
                        alignItems={'start'}
                        gap={5}
                    >
                        <VStack alignItems={'start'} gap={5} maxW={800}>
                            loading...
                        </VStack>
                    </Flex>
                </Container>
            </Box>
        );
    }

    if (isError) {
        return (
            <Container>
                <Flex
                    flexDirection={{ base: 'column-reverse', lg: 'row' }}
                    justifyContent={'space-between'}
                    alignItems={'start'}
                    gap={5}
                >
                    error fetching game trailers
                </Flex>
            </Container>
        );
    }

    return (
        <Box minHeight={'100vh'} position={'relative'} pt={32}>
            <Box
                sx={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    width: '100%',
                    height: '100%',
                    bgImage: game.background_image,
                    bgPosition: 'center',
                    bgRepeat: 'no-repeat',
                    bgSize: 'cover',
                    _before: {
                        content: '" "',
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: '100%',
                        bgColor: '#111111a2',
                    },
                }}
            />
            <Container>
                <Flex
                    flexDirection={{ base: 'column-reverse', lg: 'row' }}
                    justifyContent={'space-between'}
                    alignItems={'start'}
                    gap={5}
                >
                    <VStack alignItems={'start'} gap={5} maxW={800}>
                        <Text
                            fontSize='6xl'
                            color='#f7f7f7'
                            lineHeight={1}
                            fontWeight={'bold'}
                        >
                            {game.name}
                        </Text>

                        <HStack gap={2}>
                            {game.parent_platforms.map((platform) => (
                                <PlatformIcon
                                    key={platform.platform.id}
                                    slug={platform.platform.slug}
                                    width={1.4}
                                />
                            ))}
                            <Text
                                fontSize='sm'
                                color='#f7f7f7'
                                lineHeight={1}
                                fontWeight={'bold'}
                                borderColor={'white'}
                                borderRadius={10}
                                borderWidth={1}
                                px={2}
                                py={1}
                            >
                                {`Playtime: ${game.playtime} Hours`}
                            </Text>
                        </HStack>

                        <Box>
                            <Text
                                fontSize='2xl'
                                color='#f7f7f7'
                                fontWeight={'600'}
                            >
                                About
                            </Text>

                            <Text
                                fontSize='medium'
                                color='#f7f7f7'
                                dangerouslySetInnerHTML={{
                                    __html: game.description,
                                }}
                            />
                        </Box>
                    </VStack>

                    <VStack w={'100%'} h={'100%'}>
                        <video
                            ref={videoRef}
                            preload='none'
                            muted
                            autoPlay
                            poster={`${game.clip.preview}`}
                            style={{
                                borderRadius: 10,
                                backgroundColor: '#111111',
                                width: '100%',
                                height: '100%',
                                objectFit: 'cover',
                                objectPosition: 'center',
                            }}
                        >
                            <source
                                src={`${game.clip.clip}`}
                                style={{ width: '100%', height: '100%' }}
                            />
                            <track
                                src='/path/to/captions.vtt'
                                kind='subtitles'
                                srcLang='en'
                                label='English'
                            />
                            Your browser does not support the video tag.
                        </video>
                    </VStack>
                </Flex>
            </Container>
        </Box>
    );
};

export default Hero;
