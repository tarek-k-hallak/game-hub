// ** Components
import Container from '@/components/Container';

const Footer = () => {
    return (
        <Container as={'footer'} maxWidth={'8xl'} py={1}>
            Footer
        </Container>
    );
};

export default Footer;
