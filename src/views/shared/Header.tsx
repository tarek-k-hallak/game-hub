import { useContext, useEffect, useState } from 'react';
import { GamesFilterContext } from '@/context/GamesFilterContext';

// ** Components
import NavBar from './SideNav';
import ThemeSwitcher from '@/components/ThemeSwitcher';
import Container from '@/components/Container';
import { Icon } from '@iconify/react';
import applicationLogo from '@/assets/applicationLogo.webp';
import {
    Box,
    Flex,
    Image,
    Input,
    InputGroup,
    InputLeftElement,
    Show,
    useColorMode,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';

export default function Header() {
    // ** States
    const [bgColor, setBgColor] = useState('');

    const { setFilter } = useContext(GamesFilterContext);
    const { colorMode } = useColorMode();

    // ** Functions
    const handleChangeFilter = (e: any) => {
        setFilter('search', e.target.value);
    };

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY < 50) setBgColor('transparent');
            else setBgColor('#111111');
        };

        window.addEventListener('scroll', handleScroll);

        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    return (
        <Box
            as={'header'}
            position='fixed'
            w={'100%'}
            bgColor={bgColor}
            zIndex={1}
        >
            <Container py={4}>
                <Flex
                    justifyContent={'space-between'}
                    alignItems={'center'}
                    gap={5}
                >
                    <Link to={'/'}>
                        <Image
                            src={applicationLogo}
                            width={'2rem'}
                            cursor={'pointer'}
                            filter={`${
                                colorMode === 'dark' &&
                                'brightness(0) invert(1)'
                            }`}
                            objectFit='cover'
                            alt='game hub logo'
                        />
                    </Link>
                    <InputGroup>
                        <InputLeftElement pointerEvents='none'>
                            <Icon
                                icon='material-symbols:search'
                                width={'1.2rem'}
                            />
                        </InputLeftElement>
                        <Input
                            type='search'
                            placeholder='Search Games...'
                            borderRadius={'1rem'}
                            bgColor={'#fefefe18'}
                            onChange={handleChangeFilter}
                        />
                    </InputGroup>
                    <ThemeSwitcher />
                    <Icon icon='material-symbols:person' width={'2rem'} />
                    <Show breakpoint='(max-width: 991px)'>
                        <NavBar />
                    </Show>
                </Flex>
            </Container>
        </Box>
    );
}
