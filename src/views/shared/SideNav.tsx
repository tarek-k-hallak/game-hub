// ** React
import { useRef } from 'react';

// ** Components
import {
    Button,
    Drawer,
    DrawerBody,
    DrawerCloseButton,
    DrawerContent,
    DrawerHeader,
    DrawerOverlay,
    useDisclosure,
} from '@chakra-ui/react';
import { Icon } from '@iconify/react';

export default function NavBar() {
    // ** States
    const btnRef = useRef<HTMLButtonElement>(null);
    const { isOpen, onOpen, onClose } = useDisclosure();

    return (
        <>
            <Button as={'div'} variant={''}>
                <Icon
                    icon={'ph:hamburger'}
                    cursor={'pointer'}
                    onClick={onOpen}
                    width={'3rem'}
                />
            </Button>

            <Drawer
                isOpen={isOpen}
                placement='right'
                size={'xs'}
                onClose={onClose}
                finalFocusRef={btnRef}
            >
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Home</DrawerHeader>

                    <DrawerBody></DrawerBody>
                </DrawerContent>
            </Drawer>
        </>
    );
}
