/// <reference types="vitest/config" />

import { defineConfig } from 'vite';

import path from 'path';

export default defineConfig({
    test: {
        environment: 'jsdom',
        globals: true,
        css: true,
        alias: {
            '@': path.resolve(__dirname, './src'),
        },
        setupFiles: ['./src/test/setup.ts'],
    },
});
